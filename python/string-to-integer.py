def getIntByNumber(number, isNegative):
    if isNegative:
        if number > 2147483648:
            return -2147483648
        return number * -1
    if number > 2147483647:
        return 2147483647
    return number

def atoi(str):
    tempStr = str.strip(" ");
    tempValue = 0
    isNegative = False
    if len(tempStr) == 0:
        return 0
    if tempStr[0] == "-":
        isNegative = True
    elif tempStr[0] == "+":
        isNegative = False
    elif tempStr[0] >= "0" and tempStr[0] <= "9":
        tempValue = int(tempStr[0])
    else:
        return 0
    for i in range(1, len(tempStr)):
        if tempStr[i] >= "0" and tempStr[i] <= "9":
            tempValue = tempValue * 10 + int(tempStr[i])
        else:
            return getIntByNumber(tempValue, isNegative)
    return getIntByNumber(tempValue, isNegative)


print atoi("      -11919730356x")