def countAndSay(n):
    if n == 1:
        return "1"
    else:
        tempValue = 1
        for i in range(1, n):
            tempStr = str(tempValue)
            strLen = len(tempStr)
            if strLen == 1:
                tempValue = "1" + tempStr
            else:
                tempArr = []
                idx = 0
                for item in tempStr:
                    if len(tempArr) == 0:
                        tempArr.append({item: 1})
                    else:
                        lastDict = tempArr[idx];
                        if lastDict.has_key(item):
                            lastValue = lastDict[item]
                            newValue = lastValue + 1
                            tempArr[idx] = {item: newValue}
                        else:
                            tempArr.append({item: 1})
                            idx += 1
                resultStr = "";
                for dict in tempArr:
                    tempItem = str(dict.values()[0]) + str(dict.keys()[0])
                    resultStr += tempItem
                tempValue = resultStr
        return tempValue

print countAndSay(4)