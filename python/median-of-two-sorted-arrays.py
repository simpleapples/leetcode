def findMedianSortedArrays(A, B):
    tempArr = A + B
    tempArr.sort()
    tempResult = 0
    if len(tempArr) % 2 == 0:
        tempResult = (tempArr[len(tempArr) / 2] + tempArr[len(tempArr) / 2 - 1]) / 2.0;
    else:
        tempResult = tempArr[int(len(tempArr) / 2)]
    return tempResult

a = []
b = [2, 3]
# a = [1, 6, 9]
# b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
print findMedianSortedArrays(a, b)