def reverse(x):
    integerStr = str(x)
    if x < 0:
        integerStr = str(-x)
    resultStr = ""
    for i in range(len(integerStr) - 1, -1, -1):
        resultStr += integerStr[i]
    if x < 0:
        return int(resultStr) * -1
    return int(resultStr)

print reverse(-12312)