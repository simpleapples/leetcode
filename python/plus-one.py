def plusOne(digits):
    arrLen = len(digits)
    tempValue = 0
    if arrLen == 1:
        tempValue = digits[0] + 1
    else:
        for i in range(0, arrLen):
            tempValue += 10 ** (arrLen - 1 - i) * digits[i]
        tempValue += 1
    tempArr = []
    tempStr = str(tempValue)
    for j in range(0, len(tempStr)):
        tempArr.append(int(tempStr[j]))
    return tempArr

print plusOne([9])