def removeElement(A, elem):
    if A and len(A) == 0:
        return len(A)
    else:
        idx = 0
        while idx < len(A):
            if A[idx] == elem:
                del A[idx]
            else:
                idx += 1
        return len(A)

print removeElement([3, 3], 3)