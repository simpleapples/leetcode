class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

def isSameTree(p, q):
    if p and q:
        if p.val == q.val:
            return (isSameTree(p.left, q.left) and isSameTree(p.right, q.right))
    elif p == None and q == None:
        return True
    return False

node1 = TreeNode(1)
node2 = TreeNode(2)
node3 = TreeNode(3)
node1.left = node2
node2.right = node3
print isSameTree(node1, node2)