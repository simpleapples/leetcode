class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

def maxDepth(root):
    if root == None:
        return 0
    if root.left == None and root.right == None:
        return 1
    else:
        leftLen = 0
        rightLen = 0
        if root.left:
            leftLen = maxDepth(root.left)
        if root.right:
            rightLen = maxDepth(root.right)
        if leftLen >= rightLen:
            return leftLen + 1
        else:
            return rightLen + 1


node1 = TreeNode(1)
node2 = TreeNode(2)
node3 = TreeNode(3)
node1.left = node2
node2.right = node3
print maxDepth(node1)