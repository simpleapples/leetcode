def print_field(data):
	for idx, row in enumerate(data):
		row_string = ''
		for idx, item in enumerate(row):
			item_string = '●' if item else '○'
			row_string += ' ' + item_string
		print(row_string)

def check_complete(data):
	count = 0
	row_count = 0
	col_count = 0
	for idx, row in enumerate(data):
		row_count += 1
		for idx, item in enumerate(row):
			if idx == 0:
				col_count += 1
			count += item
	if count == row_count * col_count or count == 0:
		return True
	return False

def main():
	data = [[0, 1, 1, 0],
			[0, 0, 1, 0],
			[0, 1, 1, 0],
			[0, 1, 1, 1]]
	print_field(data)

if __name__ == '__main__':
	main()
