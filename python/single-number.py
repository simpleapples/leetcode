def singleNumber(A):
    dict = {}
    for item in A:
        if dict.has_key(item):
            dict[item] += 1
        else:
            dict[item] = 1
    for (k, v) in dict.items():
        if v == 1:
            return k

print singleNumber([1, 1, 2, 3, 3])